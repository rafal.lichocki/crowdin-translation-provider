import time
import os
import csv
from colorama import Fore, Style
import copy
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver


"""
HOWTO
Confluence doc: https://cdprojekt.atlassian.net/wiki/spaces/OS/pages/502005804/Script+for+applying+translations+to+Crowdin
Step 1: Have a "translations.csv" file on your desktop, with a format as file attached on confluence
Step 2: Run the script (provide_translations())

Note: This script assumes that the names of translations (EN slugs) are EQUAL to those in crowdin.
If there is mismatch, you need to fix that manually, either by changing translations.csv or slugs on crowdin
"""


def configuration():
    """ Configuration file for the script """
    EMAIL = ""  # provide Crowdin account email
    PASSWORD = ""  # provide Crowdin account password
    redlauncher = True  # change to False if you want to provide translations for Galaxy - long time not used
    return {"email": EMAIL, "password": PASSWORD, "redlauncher": redlauncher}


desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
driver = WebDriver()
languages = {"FR": "French",
             "IT": "Italian",
             "DE": "German",
             "ES": "Spanish",
             "AR": "Arabic",
             "CS-CZ": "Czech",
             "HU": "Hungarian",
             "JP": "Japanese",
             "KO": "Korean",
             "PL": "Polish",
             "PT-BR": "Portuguese, Brazilian",
             "RU": "Russian",
             "ZHS": "Chinese Simplified",
             "ES-MX": "Spanish, Mexico",
             "TH": "Thai",
             "ZHT": "Chinese Traditional",
             "TR": "Turkish",
             "UA": "Ukrainian",
             "PT": "Portuguese"
             }

with open(desktop + "\\translations.csv", "r",
          encoding="utf8") as file:
    language_codes = csv.DictReader(file).fieldnames
    language_codes.remove("EN")
    print("Languages detected in translation file:\n", language_codes)


def handle_csv_file(language):
    with open(desktop + "\\translations.csv", "r",
              encoding="utf8") as file:
        csv_file = csv.DictReader(file)
        source_slugs = []
        translations_slugs = []
        for row in csv_file:
            source_slugs.append(row["EN"])
            translations_slugs.append(row[language])
    return source_slugs, translations_slugs


def create_translation_dict(source_slugs, translations_slugs, language_code):
    translation_pair_dict = {}
    counter = 0

    print("Creating a dict with translations for language %s..." % language_code)
    for translation in source_slugs:
        translation_pair_dict[translation] = translations_slugs[counter]
        counter += 1
    print("Dict creation completed. Printing result:\n", translation_pair_dict)
    print("\n")
    return translation_pair_dict


def access_crowdin(redlauncher, email, password):
    if redlauncher is True:
        driver.get("https://crowdin.com/translate/gog-galaxy/786/en-ar?filter=basic&value=0")  # redlauncher url
    else:
        driver.get("https://crowdin.com/translate/gog-galaxy/625/en-pl?filter=basic&value=0")  # galaxy url
    time.sleep(3)
    driver.find_element(By.ID, "login_login").send_keys(email)
    driver.find_element(By.ID, "login_password").send_keys(password)
    driver.find_element(By.XPATH, "//button[@type='submit']").click()
    driver.find_element(By.ID, "remember-not-now").click()
    time.sleep(3)


def select_language():
    language = languages[initial_language_in_translation_file()]
    driver.find_element(By.XPATH, "//a[@class='btn mdc-button "
                                  "open-language-menu']").click()
    lang_filter_textfield = driver.find_element(By.ID, "menu-filter-languages")
    lang_filter_textfield.click()
    lang_filter_textfield.send_keys(language)
    time.sleep(2)
    xpath_elements = driver.find_elements(By.XPATH,
                                          "//a[@class='translation-table-language-link text-overflow']")
    print("Selecting starting language in Crowdin.")
    for element in xpath_elements:
        if element.text == language:
            element.click()
            time.sleep(1)
            print("%s selected." % language)
            break
    print(" \n")


def initial_language_in_translation_file():
    with open(desktop + "\\translations.csv", "r",
              encoding="utf8") as file:
        language_codes = csv.DictReader(file).fieldnames
        language_codes.remove("EN")
        return language_codes[0]


def change_language(language):
    try:
        driver.find_element(By.XPATH, "//div[@class='jGrowl-close unselectable']").click()
        print("Identical translation already present. Moving on")
    except NoSuchElementException:
        "Everything in order"
    driver.find_element(By.XPATH, "//a[@class='btn mdc-button "
                                  "open-language-menu']").click()
    xpath_elements = driver.find_elements(By.XPATH, "//a["
                                                    "@class='translation-table"
                                                    "-language-link text-overflow']")
    print("Changing language for translations.")
    for element in xpath_elements:
        if element.text == languages[language]:
            element.click()
            time.sleep(1)
            break
    print("%s selected." % language)
    print(" \n")


def access_game_view_translations():
    driver.find_element(By.ID, "file-menu-item").click()
    node_list = driver.find_elements(By.XPATH, "//span[@class='node_name has_hint']")
    for node in node_list:
        if node.text == "Galaxy 2.0 general":
            print("Accessing Galaxy 2.0 gameView tab")
            node.click()
            break
    buttons = driver.find_elements(By.XPATH, "//button[@type='button']//"
                                             "span[@class='ui-button-text']")
    buttons[15].click()


def prepare_slug(slug):
    prepared_slug = slug.replace("\n", " ")
    return prepared_slug


def find_slug(slug):
    driver.find_element(By.ID, "editor_search_bar").send_keys(slug)
    driver.find_element(By.ID, "editor_search_bar").send_keys(Keys.ENTER)
    time.sleep(3)


def clear_search_bar():
    driver.find_element(By.ID, "editor_search_bar").clear()


def fill_translations(translation_pair_dict):
    translations_to_fill_dict = copy.deepcopy(translation_pair_dict)
    for key, value in translation_pair_dict.items():
        prepared_slug = prepare_slug(key)
        find_slug(prepared_slug)
        untranslated_phrases = driver.find_elements(By.XPATH, "//a[@class='source-string ']")
        if value == "":
            print(Fore.YELLOW + "Empty translation detected, please check the translation file" + Style.RESET_ALL)
        else:
            del_counter = 0
            for phrase in untranslated_phrases:
                if prepared_slug == phrase.text:
                    if del_counter < 1:
                        del translations_to_fill_dict[key]
                        del_counter += 1
                    phrase.click()
                    print("English slug: %s" % phrase.text)
                    textfield = driver.find_element(By.XPATH, "//textarea[@id='translation']")
                    textfield.clear()
                    textfield.send_keys(value)
                    print("New value: %s" % value)
                    time.sleep(1)
                    driver.find_element(By.ID, "suggest_translation").click()  # submit translation button, weird ID
                    time.sleep(5)
                    try:
                        try:
                            driver.save_screenshot("error.png")
                            print(Fore.RED + driver.find_element(By.XPATH, "//p[@class='notification-body']").text
                                  + Style.RESET_ALL)
                            driver.find_element(
                                By.XPATH, "//button[@class='btn btn-small confirm-notification ']").click()
                            print(Fore.RED + "Problem encountered. Saving anyway (might require manual inspection)"
                                  + Style.RESET_ALL)
                            time.sleep(1)
                        except NoSuchElementException:
                            "Couldn't save anyway, attempting to cancel"
                        driver.find_element(
                            By.XPATH, "//button[@class='btn btn-small confirm-notification']").click()
                    except NoSuchElementException:
                        "Everything in order"
                else:
                    pass
        clear_search_bar()
    if len(translations_to_fill_dict) > 0:
        print(Fore.RED + "Translations that were not found on the crowdin list:" + Style.RESET_ALL)
        for key in translations_to_fill_dict:
            print(f"{key}\n")
    else:
        print(Fore.GREEN + "All translations were found and applied" + Style.RESET_ALL)
    print("")


def provide_translations():
    config = configuration()
    access_crowdin(redlauncher=config["redlauncher"], email=config["email"], password=config["password"])
    select_language()
    language_counter = 0
    for language_code in language_codes:
        # change_language function needs to fire only after the first round, hence counter
        if language_counter > 0:
            change_language(language_code)
        slugs_from_csv = handle_csv_file(language_code)
        translation_dict = create_translation_dict(slugs_from_csv[0], slugs_from_csv[1], language_code)
        fill_translations(translation_dict)
        language_counter += 1
    driver.quit()


provide_translations()
