Requirements: 

- Python 3.9
- selenium 4.9
- colorama 0.4.6
- chromedriver ChromeDriver - WebDriver for Chrome - Getting started (or driver for other browser, I was using it on Chrome and can confirm it works on this browser)
 

How to run:
1. Have a "translations.csv" file on your desktop, with a format as file attached on confluence
2. Provide email and password in the configuration function
3. Run the script (provide_translations function to be precise, but script calls it on its own)


Format for translation file can be seen on Confluence page: https://cdprojekt.atlassian.net/wiki/spaces/OS/pages/502005804/Script+for+applying+translations+to+Crowdin
